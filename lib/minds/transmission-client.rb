require 'net/http'
require 'singleton'
require 'rubygems'
require 'json'

require_relative('transmission-client/client')

if (defined?(EM))
    begin
        require('em-http')
        require_relative('transmission-client/em-connection')
    rescue LoadError
        require_relative('transmission-client/connection')
    end
else
    require_relative('transmission-client/connection')
end
require_relative('transmission-client/torrent')
require_relative('transmission-client/session')

