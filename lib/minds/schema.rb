require("sequel")

module Minds
    class Schema
        attr_reader(:db)

        #constructor
        def initialize(kwargs={})
            if (!kwargs.has_key?(:dsn))
                raise(ArgumentError, "missing dsn", caller)
            end

            #connect to database
            @db = Sequel.connect(kwargs[:dsn])   

            #load model's if needed
            self._load_models
            
            #create instance models
            self._create_instance_models
        end

        def self.search_dirs()
            return []
        end

        def method_missing(sym, *args, &block)
            mdl = ("M" + self.object_id.to_s).to_sym
            mdl = self.class.const_get(mdl)
            
            return mdl.const_get(sym)
        end
        
        # Protected methods
        protected

        def _create_instance_models()
            #model namespace
            mdl = Module.new()
            self.class.const_set(("M" + self.object_id.to_s).to_sym, mdl)
            
            #create model's
            self.class.constants.each do |cls|
                if (cls.to_s.match(/^M\d+$/i))
                    next
                end
                
                #create model based on the original
                cls_obj = self.class.const_get(cls)
                subclass = Class.new(cls_obj)
                mdl.const_set(cls, subclass)

                if (@db != nil)
                    subclass.db = @db
                end
            end
            
            #run __setup__
            mdl.constants.each do |cls|
                cls_obj = mdl.const_get(cls)

                #check for __setup__ method
                if (cls_obj.respond_to?(:__setup__))
                    cls_obj.__setup__(mdl)
                end
            end

            return
        end

        def _load_models()
            #if models were already loaded don't do nothing
            if (self.class.constants.length > 0)
                return
            end

            #load model's
            self.class.search_dirs.each do |dir|
                files = Dir.glob("#{ dir }/*.rb")
                files.each do |file|
                    contents = File.readlines(file)
                    contents = contents.join("")
                    self.class.class_eval(contents)
                end
            end

            return
        end
    end
end
