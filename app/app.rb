#!/usr/bin/env rvm-shebang-ruby
# coding: utf-8

#change working directory if needed
cur_file = File.expand_path(__FILE__)
if (File.symlink?(cur_file))
    cur_file = File.expand_path(File.readlink(cur_file))
end
WORKING_DIR = File.dirname(cur_file)
Dir.chdir(WORKING_DIR)

##requires
require('log4r')
require('flt')
require('highline')

#lib paths
$LOAD_PATH.unshift(WORKING_DIR + '/../lib')

require("minds/terminal/shell")
require("minds/transmission-client")
require('minds/config')

class TransmissionShell < Minds::Terminal::Shell
    attr_reader(:transmission, :config)

    #constructor
    def initialize()
        super

        @config = ::Minds::Config.create(
            :type => :YAML,
            :file => @_root + '/../etc/config.yaml'
        )

        #init transmission client
        @transmission = Transmission::Client.new(
            @config['transmission.host'],
            @config['transmission.port']
        )
    end

    protected

    def _root()
        return WORKING_DIR
    end

    def _identifier()
        return "transmission >> "
    end
end

if (__FILE__ == $0)
    TransmissionShell.start
end
