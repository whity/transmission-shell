def main(shell, *args)
    #connect transmission
    transmission = shell.transmission()

    #get torrents
    torrents = transmission.torrents()

    #stop given torrents
    args.each() do |id|
        idx = torrents.index() do |item|
            id.to_i() == item.id
        end
        if (idx == nil)
            next
        end

        torrent = torrents[idx]

        #ensure torrent is stopped
        torrent.stop()

        #remove it
        torrent.remove()

        #remove from array
        torrents.delete_at(idx)
    end

    return true
end
