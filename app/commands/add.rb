def main(shell, *args)
    #connect transmission
    transmission = shell.transmission()

    args.each() do |file|
        transmission.add_torrent_by_file(File.expand_path(file))
    end
    
    return true
end
