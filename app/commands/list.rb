require("minds/terminal/data_table")

def main(shell)
    #connect transmission
    transmission = shell.transmission
    
    #get torrents
    torrents = transmission.torrents

    #list columns to show
    columns = [
        {'name' => 'id',              'label' => 'ID',          'chars' => 5},
        {'name' => 'name',            'label' => 'Name',        'chars' => 10},
        {'name' => 'sizeWhenDone',    'label' => 'Size',        'chars' => 8},
        {'name' => 'downloaded_size', 'label' => 'Dwn size',    'chars' => 8},
        {'name' => 'percentDone',     'label' => '%',           'chars' => 4},
        {'name' => 'rateDownload',    'label' => 'Dwn rate',    'chars' => 4},
        {'name' => 'rateUpload',      'label' => 'Upl rate',    'chars' => 4},
        {'name' => 'status',          'label' => 'Status',      'chars' => 10},
        {'name' => 'eta',             'label' => 'time to end', 'chars' => 11}
    ]
    
    #prepare data to show
    data = Array.new
    torrents.each do |tor|
        row = Hash.new
        columns.each do |col|
            value = nil
            
            begin
                value = tor.send(col['name'].to_sym)
            rescue
                #don't do nothing
            end
                
            #if col is size, format it
            if (['sizeWhenDone', 'rateDownload', 'rateUpload'].include?(col['name']))
                value = _calculate_size(value)
            elsif (col['name'] == 'status')
                value = _status(value)
            elsif (col['name'] == 'percentDone')
                value = (value.to_f * 100).round(2)
            elsif (col['name'] == 'downloaded_size')
                total_size = tor.sizeWhenDone
                to_finish = tor.leftUntilDone
                value = _calculate_size(total_size - to_finish)
            elsif (col['name'] == 'eta')
                value = _get_time(value)
            end
            
            row[col['name']] = value
        end
        
        #add row
        data.push(row)
    end
    
    data_table = Terminal::DataTable.new(columns, data)
    puts(data_table.build)
    
    return true
end

def _calculate_size(size)
    result = ""
    #divisor = 8 #8 bits => 1 Byte
    #suffix = "B" #Byte
    size = Flt::DecNum(size)
    
    level = 0
    level_suffix = {
        0 => "B",
        1 => "Kb",
        2 => "Mb",
        3 => "Gb"
    }
    
    if (size > 1024)
        level = 1
        size = Flt::DecNum(size / 1024)
        while (size.to_s().match(/^[^0]/))
            size = Flt::DecNum(size / 1024)
            if (size.to_s().match(/^0/))
                size *= 1024
                break
            end
            level += 1
        end
    end
    
    return size.round(2).to_s() + level_suffix[level]
end

def _status(code)

    status = {
        0  => 'Stopped',
        1  => 'Queued to check files',
        2  => 'Checking files',
        3  => 'Queued to download',
        4  => 'Downloading',
        5  => 'Queued to seed',
        6  => 'Seeding'
    }
    
    if (status.has_key?(code))
        return status[code]
    end
    
    return code
end

def _get_time(time)
    result = ""
    
    iterations = 0
    iterations_suffix = {
        0 => 'sec(s)',
        1 => 'min(s)',
        2 => 'hour(s)',
        3 => 'day(s)'
    }
    
    while (time > 60)
        time = time / 60
        iterations += 1
    end
    
    if (time > 0)
        time = time.to_i()
        result = time.to_s() + " " + iterations_suffix[iterations]
    end
    
    return result
end
