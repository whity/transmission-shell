# transmission shell

## commands currently available

- add, add a new torrent by file name)
- rm, remove a torrent by id)
- list/ls, list the current torrents
- start, start the torrent
- stop, stop the torrent
- quit/exit, quits the app
- history, shows the commands history
- clear, clears the screen 

## installation

1. install ruby 2.0
2. install bundler
    ```shell
    gem install bundler
    ```

3. download the package and extract it to any place.
4. in the folder created by the extraction run the following command:
    ```shell
    bundler install --deployment --path=lib/vendor
    ```
5. in the folder 'etc' copy 'config.example.yaml' to 'config.yaml' and edit to 
configure the connection to transmission

## run

inside the app folder run './app/app.rb'

## how to add a new command

the commands are located in the folder 'app/commands', here is an example:
```ruby    
##hello.rb

def main(shell)
    puts('hello')

    return true
end
```
